<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php //TODO ?>
    <meta name="language" content="<?php echo Yii::app()->language ?>" />
    <meta name="viewport" content="width=device-width" />
    <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/kube.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body itemscope itemtype="http://schema.org/<?php echo $this->webpageType; ?>">
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=190173181144359";
      fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
  <div class="wrapper">
    <header itemtype="http://schema.org/WPHeader">
        <img src="<?php echo Yii::app()->baseUrl; ?>/logo.png" class="left">
        <a href="<?php echo Yii::app()->baseUrl; ?>/">
            <h1 class="head"><?php echo Settings::get('site', 'name'); ?></h1>
        </a>
        <nav id="nav" itemscope itemtype="http://schema.org/SiteNavigationElement" >
            <?php $this->widget('MenuRenderer'); ?>
        </nav>
        <?php $this->widget('SearchBlock'); ?>

    </header>

    <div class="mid-bar row">
        <?php //TODO: show up some message and breadcrumb like Home /Index when user is not signed in and is in home page  ?>
        <?php
        if (((Settings::get('site', 'enable_breadcrumbs') == '') || (Settings::get('site', 'enable_breadcrumbs') == 1)) && isset($this->breadcrumbs)) {
            $this->widget('Breadcrumbs', array(
                'links' => $this->breadcrumbs,
                ));
        }
        ?>
    </div>

    <div class="row">
        <div class="fifth sidebar"itemtype="http://schema.org/WPSideBar">
           <?php $this->widget('MenuRenderer', array('id'=>3)); ?>

           <?php $this->widget('Events'); ?>

           <?php $this->widget('LatestNews'); ?>

           <div class="fb-like-box" data-href="https://www.facebook.com/hotc.nepal" data-width="160" data-height="400" data-show-faces="true" data-stream="false" data-show-border="false" data-header="false"></div>

       </div>

       <div class="fourfifth" itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">
        <?php echo $content; ?>
    </div>
</div>
<?php include_once '_footer.php'; ?>
</div>
<?php $this->widget('GAnalytics'); ?>
</body>
</html>