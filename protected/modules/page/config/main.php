<?php

return array(
    'import' => array(
        'application.modules.page.models.*',
        'application.modules.page.behaviors.*',
        'application.modules.page.widgets.*',
        'application.modules.page.components.*',
    ),
);
